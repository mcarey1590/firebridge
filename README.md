# firebridge

[![pipeline status](https://gitlab.com/endran/firebridge/badges/develop/pipeline.svg)](https://gitlab.com/endran/firebridge/commits/master)
[![licence](https://img.shields.io/npm/l/@endran/firebridge.svg?style=flat)](https://opensource.org/licenses/MIT)
[![npm version](https://img.shields.io/npm/v/@endran/firebridge.svg?style=flat)](https://badge.fury.io/js/%40endran%2Ffirebridge)

A Node.js HTTP or CLI bridge for Firebase Admin, during **testing and development**.<br>
Gives access to Firebase via **CLI** or **HTTP**, opening up Admin power from anywhere.

## Installation

```bash
npm install @endran/firebridge --save
```

or

```bash
yarn add @endran/firebridge
```

## Usage

You can use Firebridge as a HTTP service, or as a CLI tool. Both have their own use case.

Use the **CLI** in build scripts, for example in your `package.json`.

```
firebridge --serviceAccount firebase-admin.json --commandPath 'export.command.json'
```

Use the **HTTP server** in your end to end tests, we use it in combination with [Cypress](https://www.cypress.io/), since it circumvents Firebase Admin SDK compatibility.

```
firebridge --serviceAccount firebase-admin.json --open 4201
```

## Commands

The CLI requires a path to a **command file** relative from the working directory of Firebridge. The Server accepts **POST requests**. The content of command file, or POST body is identical.

In the examples below we use `"ref":"__ROOT__"`, but instead we could also use `"ref":"col1/doc2"` to indicate a specific document or collection. `__ROOT__` is the keyword used for the root of Firestore.

### Export

The easiest way to get started is to first export a database that you have crafted manually.

```json
{
    "action": "FIRESTORE.EXPORT",
    "ref": "__ROOT__",
    "file": "export.json"
}
```

### Import

Now that we have a data set, let's import it.

```json
{
    "action": "FIRESTORE.IMPORT",
    "file": "export.json",
    "ref": "__ROOT__"
}
```

### Clear

Or even better, first wipe all data, so that we can prove import works.

```json
{
    "action": "FIRESTORE.CLEAR",
    "ref": "__ROOT__"
}
```

### Add Users

You'll need to create test accounts, let's automate this as well.

```json
{
    "action": "AUTH.ADD",
    "uid": "TESR_USER_ID_1",
    "email": "user1@email.com",
    "password": "123456"
}
```

Instead of having the `uid`, `email` and `password` details in json command, you can also have a field `"file": "users.json"` which can hold an array of users.

### Remove Users

Every once in a while you might need to remove users, because of reasons like automated testing, this is possible via below command, keep in mind that either `keep` or `remove` _regex_ must be set, or both. When both are set, first `remove` is applied, and then on that set `keep` is applied.

```json
{
    "action": "AUTH.DELETE",
    "remove": ".*@remove.com",
    "keep": ".*@keep.com"
}
```

### Get

All commands until now are using mostly config files as source data, however often you need to be able to dynamically access your data, especially during E2E tests.

```json
{
    "action": "FIRESTORE.GET",
    "ref": "some/document"
}
```

This will yield a response with a `data` field, which has the same structure as before with `FIRESTORE.EXPORT`. This means that also any collections of this document will be returned as `data.__collections__`.
You can use this result to assert the state of the database in respect to the state of the application in your test.

### Set

To make sure the application is in a certain state you must make sure Firestore is correct. For this you can use `FIRESTORE.IMPORT`, but often you need to add more data dynamically from your test.

```json
{
    "action": "FIRESTORE.SET",
    "ref": "some/document",
    "data": {"someKey": "someValue"}
}
```

This will set the provided `data` at `ref`. When data is `null` it will delete `ref`. By using the field `__collections__` you can also insert a document _with_ collections.

## Help

```
Usage: index [options]

Options:
  -V, --version                output the version number
  -S, --serviceAccount <path>  Required. Location of service account JSON file.
  -O, --open <number>          Starts applicants as server on the given port. Prevents --commandPath.
  -P, --commandPath <path>     Executes a single command from path. Is ignored when --open is used.
  --verbose                    Enable verbose logging.
  -h, --help                   output usage information

Examples:
  $ firebridge -S firebase-admin.json --open 4201
  $ firebridge -S firebase-admin.json --commandPath examples/simpleSetCommand.json

Expected data format per command
AUTH.DELETE:       { "action":"AUTH.DELETE", "remove?": ".*@remove.com", "keep?": ".*@keep.com" }
AUTH.ADD:          { "action":"AUTH.ADD", "file?": "users.json", "uid?": "USER_ID_1", "email?": "user@example.com", "password?": "atLeastSixChars" }
FIRESTORE.GET:     { "action":"FIRESTORE.GET", "ref": "__ROOT__" | "colA" | "colA/docB" }
FIRESTORE.SET:     { "action":"FIRESTORE.SET", "ref": "colA/docB", "data": {} }
FIRESTORE.CLEAR:   { "action":"FIRESTORE.CLEAR", "ref": "__ROOT__" | "colA" | "colA/docB" }
FIRESTORE.IMPORT:  { "action":"FIRESTORE.IMPORT", "ref": "__ROOT__" | "colA" | "colA/docB", "file": "export.json" }
FIRESTORE.EXPORT:  { "action":"FIRESTORE.EXPORT", "ref": "__ROOT__" | "colA" | "colA/docB", "file": "export.json" }

```

## Notes

**File format:**<br>
Firebridge uses [node-firestore-import-export](https://www.npmjs.com/package/node-firestore-import-export), this library has support for complex data types like `Timestamp` and `Geopoint`.

**Service Account:**<br>
You can get the `firebase-admin.json` file from the [Firebase Console](https://console.firebase.google.com/). Select _<your project>_, click the _cog_, go to _User and Permission_, then _Service Accounts_, tick _Node.js_, and hit _Generate_.

## Known issues

Unresponsive after system sleep:

-   For Mac users, when you close your MacBook, Firebridge may need a restart to work properly again.

## Keywords

Firebase, Cloud Firestore, Firebase Authentication, Cypress, E2E Testing, CI/CD

## License

```text
The MIT License

Copyright (c) 2019 David Hardy
Copyright (c) 2019 codecentric nl

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
```
