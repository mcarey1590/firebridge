import {FirebaseAdmin} from '../../firebase-admin';
import * as fs from 'fs';
import {firestoreExport} from 'node-firestore-import-export/dist/lib';
import {constructRef} from '../../util';
import {Command} from '../command';

export class FirestoreExportCommand implements Command {
    static KEY = 'FIRESTORE.EXPORT';
    format = ` { "action":"FIRESTORE.EXPORT", "ref": "__ROOT__" | "colA" | "colA/docB", "file": "export.json" }`;

    async execute(firebaseAdmin: FirebaseAdmin, params: any): Promise<any> {
        const file = `${process.cwd()}/${params.file}`;
        if (fs.existsSync(file)) {
            return {error: `File ${file} already exists`};
        }

        fs.openSync(file, 'w');

        let data: any;
        const ref = constructRef(firebaseAdmin.firestore, params.ref);
        data = await this.export(ref);

        if (!data) {
            return {error: 'Could not export data'};
        }

        console.log(`Writing data to ${params.file}`);

        fs.writeFileSync(file, JSON.stringify(data));

        return {success: true};
    }

    async export(ref: any): Promise<any> {
        return await firestoreExport(ref);
    }
}
