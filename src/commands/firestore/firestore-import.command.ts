import {FirebaseAdmin} from '../../firebase-admin';
import * as fs from 'fs';
import {firestoreImport} from 'node-firestore-import-export/dist/lib';
import {constructRef} from '../../util';
import {Command} from '../command';

export class FirestoreImportCommand implements Command {
    static KEY = 'FIRESTORE.IMPORT';
    format = ` { "action":"FIRESTORE.IMPORT", "ref": "__ROOT__" | "colA" | "colA/docB", "file": "export.json" }`;

    async execute(firebaseAdmin: FirebaseAdmin, params: any): Promise<any> {
        const file = `${process.cwd()}/${params.file}`;
        if (!fs.existsSync(file)) {
            return {
                error: `File ${file} does not exist`
            };
        }

        const data = require(file);

        const ref = constructRef(firebaseAdmin.firestore, params.ref);
        await this.import(ref, data);

        return {
            success: true
        };
    }

    async import(ref: any, data: any): Promise<any> {
        await firestoreImport(data, ref);
    }
}
