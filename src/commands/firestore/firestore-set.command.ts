import {FirebaseAdmin} from '../../firebase-admin';
import {Command} from '../command';
import {firestoreImport} from 'node-firestore-import-export/dist/lib';
import {constructRef} from '../../util';

export class FirestoreSetCommand implements Command {
    static KEY = 'FIRESTORE.SET';
    format = `    { "action":"FIRESTORE.SET", "ref": "colA/docB", "data": {} }`;

    async execute(firebaseAdmin: FirebaseAdmin, params: any): Promise<any> {
        const ref = constructRef(firebaseAdmin.firestore, params.ref);

        if (params.data) {
            await this.import(ref, {__collections__: {}, ...params.data});
        } else {
            await firebaseAdmin.firestore.doc(params.ref).delete();
        }

        return {
            success: true
        };
    }

    async import(ref: any, data: any): Promise<any> {
        await firestoreImport(data, ref);
    }
}
