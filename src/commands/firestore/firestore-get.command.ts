import {FirebaseAdmin} from '../../firebase-admin';
import {Command} from '../command';
import {constructRef, getInformedPath} from '../../util';
import * as fs from 'fs';
import {firestoreExport} from 'node-firestore-import-export/dist/lib';

export class FirestoreGetCommand implements Command {
    static KEY = 'FIRESTORE.GET';
    format = `    { "action":"FIRESTORE.GET", "ref": "__ROOT__" | "colA" | "colA/docB" }`;

    async execute(firebaseAdmin: FirebaseAdmin, params: any): Promise<any> {
        let data: any;
        const ref = constructRef(firebaseAdmin.firestore, params.ref);
        data = await this.export(ref);

        if (!data) {
            return {error: 'Could not export data'};
        }
        return {success: true, data};
    }

    async export(ref: any): Promise<any> {
        return await firestoreExport(ref);
    }
}
