import {firestoreClear} from 'node-firestore-import-export';

import {FirebaseAdmin} from '../../firebase-admin';
import {constructRef} from '../../util';
import {Command} from '../command';

export class FirestoreClearCommand implements Command {
    static KEY = 'FIRESTORE.CLEAR';
    format = `  { "action":"FIRESTORE.CLEAR", "ref": "__ROOT__" | "colA" | "colA/docB" }`;

    async execute(firebaseAdmin: FirebaseAdmin, params: any): Promise<any> {
        const ref = constructRef(firebaseAdmin.firestore, params.ref);
        await this.clear(ref);

        return {
            success: true
        };
    }

    async clear(
        collectionReferences: FirebaseFirestore.Firestore | FirebaseFirestore.DocumentReference | FirebaseFirestore.CollectionReference
    ): Promise<any> {
        await firestoreClear(collectionReferences);
    }
}
